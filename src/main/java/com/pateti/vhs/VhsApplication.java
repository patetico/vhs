package com.pateti.vhs;

import com.pateti.vhs.domain.dao.IDaoImplFactory;
import com.pateti.vhs.domain.dao.sqlite.DaoImplFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import java.security.InvalidParameterException;

@SpringBootApplication
public class VhsApplication {

    @Bean
    public IDaoImplFactory getBancoFactory(@Value("${vhs.dao.banco}") String tipoBanco, Environment env) {
        if ("sqlite".equals(tipoBanco)) {
            return new DaoImplFactory(env);
        }
        throw new InvalidParameterException(String.format("Tipo desconhecido de banco de dados: %s", tipoBanco));
    }

    public static void main(String[] args) {
        SpringApplication.run(VhsApplication.class, args);
    }

}
