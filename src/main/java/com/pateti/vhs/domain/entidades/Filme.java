package com.pateti.vhs.domain.entidades;

import java.security.InvalidParameterException;

public class Filme {
    private int id;
    private String titulo;
    private String tituloOriginal;
    private String sinopse;
    private int duracao;  // em minutos
    private int censura;  // idade mínima
    private int categoriaId;
    private Categoria categoria;

    public Filme(int id, String titulo) {
        this.id = id;
        this.setTitulo(titulo);
    }

    public int getId() {
        return id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        if (titulo == null) throw new InvalidParameterException("Título não pode ser nulo");
        if (titulo.isBlank()) throw new InvalidParameterException("Título não pode ser vazio");

        this.titulo = titulo.trim();
    }

    public String getTituloOriginal() {
        return tituloOriginal;
    }

    public void setTituloOriginal(String tituloOriginal) {
        this.tituloOriginal = tituloOriginal;
    }

    public Categoria getCategoria() {
        if (categoriaId > 0 && categoria == null) {
            // TODO: buscar categoria no banco
        }
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoriaId = categoria != null ? categoria.getId() : -1;
        this.categoria = categoria;
    }

    public String getSinopse() {
        return sinopse;
    }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    public int getCensura() {
        return censura;
    }

    public void setCensura(int censura) {
        this.censura = censura;
    }

    public int getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(int categoriaId) {
        if (this.categoriaId != categoriaId) {
            this.categoriaId = categoriaId;
            this.categoria = null;
        }
    }
}
