package com.pateti.vhs.domain.entidades;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class Fita {
    private int id;
    private int filmeId;
    @NotNull
    private Filme filme;
    private boolean rebobinada;
    private String avarias;

    public Fita(int id, int filmeId) {
        this.id = id;
        this.setFilmeId(filmeId);
    }

    public int getId() {
        return id;
    }

    public Filme getFilme() {
        if (filmeId > 0 && filme == null) {
            // TODO: buscar filme no banco
        }
        return filme;
    }

    public void setFilme(@NotNull Filme filme) {
        this.filme = Objects.requireNonNull(filme, "Filme não pode ser nulo");
        this.filmeId = filme.getId();
    }

    public boolean isRebobinada() {
        return rebobinada;
    }

    public void setRebobinada(boolean rebobinada) {
        this.rebobinada = rebobinada;
    }

    public String getAvarias() {
        return avarias;
    }

    public void setAvarias(String avarias) {
        this.avarias = avarias;
    }

    public int getFilmeId() {
        return filmeId;
    }

    public void setFilmeId(int filmeId) {
        if (this.filmeId != filmeId) {
            this.filmeId = filmeId;
            this.filme = null;
        }
    }
}
