package com.pateti.vhs.domain.entidades;

import br.com.caelum.stella.format.CPFFormatter;
import br.com.caelum.stella.validation.CPFValidator;
import br.com.caelum.stella.validation.InvalidStateException;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class Cliente {
    private static CPFValidator cpfValidator = new CPFValidator();
    private static CPFFormatter cpfFormatter = new CPFFormatter();

    private int id;
    private String nome;
    private String cpf;
    private BigDecimal saldo;


    public Cliente(int id, String nome, String cpf) {
        this.setId(id);
        this.setNome(nome);
        this.setCpf(cpf);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) throws InvalidStateException {
        final String unformatted = cpfFormatter.unformat(cpf);
        cpfValidator.assertValid(unformatted);
        this.cpf = cpfFormatter.format(unformatted);
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(int centavos) {
        setSaldo(new BigDecimal(centavos));
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo.setScale(2, RoundingMode.FLOOR);
    }
}
