package com.pateti.vhs.domain.entidades;

public class Categoria {
    private int id;

    private String name;
    public Categoria(int id, String name) {
        this.id = id;
        this.setName(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
