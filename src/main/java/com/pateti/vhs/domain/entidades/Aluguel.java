package com.pateti.vhs.domain.entidades;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.security.InvalidParameterException;
import java.util.Date;
import java.util.Objects;


public class Aluguel {
    private int id;
    private int fitaId;
    private Fita fita;
    private int clienteId;
    private Cliente cliente;
    private Date retirada;
    private Date devolucao;
    private BigDecimal taxaDiaria;
    private BigDecimal valorTotal;
    private boolean pago;

    public Aluguel(int id, int fitaId, int clienteId, Date retirada, int taxaDiariaCentavos) {
        this.id = id;
        this.setFitaId(fitaId);
        this.setClienteId(clienteId);
        this.setRetirada(retirada);
        this.setTaxaDiaria(taxaDiariaCentavos);
        this.setPago(false);
    }

    public Aluguel(int id, int fitaId, int clienteId, Date retirada, BigDecimal taxaDiaria) {
        this.id = id;
        this.setFitaId(fitaId);
        this.setClienteId(clienteId);
        this.setRetirada(retirada);
        this.setTaxaDiaria(taxaDiaria);
        this.setPago(false);
    }

    public int getId() {
        return id;
    }

    public int getFitaId() {
        return fitaId;
    }

    public void setFitaId(int fitaId) {
        if (this.fitaId != fitaId) this.fita = null;
        this.fitaId = fitaId;
    }

    public Fita getFita() {
        if (fitaId > 0 && fita == null) {
            // TODO: buscar fita no banco
        }
        return fita;
    }

    public void setFita(@NotNull Fita fita) {
        this.fita = Objects.requireNonNull(fita, "Fita não pode ser nula");
        this.fitaId = fita.getId();
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        if (this.clienteId != clienteId) this.cliente = null;
        this.clienteId = clienteId;
    }

    public Cliente getCliente() {
        if (clienteId > 0 && cliente == null) {
            // TODO: buscar cliente no banco
        }
        return cliente;
    }

    public void setCliente(@NotNull Cliente cliente) {
        this.cliente = Objects.requireNonNull(cliente, "Cliente não pode ser nulo.");
        this.clienteId = cliente.getId();
    }

    public Date getRetirada() {
        return retirada;
    }

    public void setRetirada(Date retirada) {
        this.retirada = retirada;
    }

    public Date getDevolucao() {
        return devolucao;
    }

    public void setDevolucao(Date devolucao) {
        if (devolucao.before(retirada))
            throw new InvalidParameterException("Não é possível devolver uma fita antes da sua retirada");
        this.devolucao = devolucao;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(int centavos) {
        MathContext mc = new MathContext(2, RoundingMode.CEILING);
        this.valorTotal = new BigDecimal(centavos, mc).movePointLeft(2);
    }

    public void setValor(BigDecimal valor) {
        if (valor.signum() < 0) throw new IllegalArgumentException("Valor não pode ser negativo");
        this.valorTotal = valor.setScale(2, RoundingMode.CEILING);
    }

    public boolean isPago() {
        return pago;
    }

    public void setPago(boolean pago) {
        this.pago = pago;
    }

    public BigDecimal getTaxaDiaria() {
        return taxaDiaria;
    }

    public void setTaxaDiaria(BigDecimal taxaDiaria) {
        this.taxaDiaria = taxaDiaria;
    }

    private void setTaxaDiaria(int taxaCentavos) {
        this.taxaDiaria = new BigDecimal(taxaCentavos).setScale(2, RoundingMode.CEILING).movePointLeft(2);
    }
}
