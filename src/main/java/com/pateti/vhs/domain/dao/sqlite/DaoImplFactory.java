package com.pateti.vhs.domain.dao.sqlite;

import com.pateti.vhs.domain.dao.IDaoBase;
import com.pateti.vhs.domain.dao.IDaoImplFactory;
import com.pateti.vhs.domain.dao.sqlite.impl.*;
import com.pateti.vhs.domain.entidades.*;
import org.springframework.core.env.Environment;


public class DaoImplFactory implements IDaoImplFactory {
    private Environment env;

    public DaoImplFactory(Environment env) {
        this.env = env;
    }

    public IDaoBase<Aluguel> createAluguelDAO() {
        return new AluguelDaoImpl(env);
    }

    public IDaoBase<Categoria> createCategoriaDAO() {
        return new CategoriaDaoImpl(env);
    }

    public IDaoBase<Cliente> createClienteDAO() {
        return new ClienteDaoImpl(env);
    }

    public IDaoBase<Filme> createFilmeDAO() {
        return new FilmeDaoImpl(env);
    }

    public IDaoBase<Fita> createFitaDAO() {
        return new FitaDaoImpl(env);
    }
}
