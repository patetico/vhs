package com.pateti.vhs.domain.dao;

import java.util.List;


public interface IDaoBase<T> {
    int inserir(T ent);

    boolean atualizar(T ent);

    boolean remover(int id);

    List<T> listar();

    T consultar(int id);
}
