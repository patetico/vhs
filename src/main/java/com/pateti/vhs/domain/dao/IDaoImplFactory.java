package com.pateti.vhs.domain.dao;

import com.pateti.vhs.domain.entidades.*;

public interface IDaoImplFactory {
    public IDaoBase<Aluguel> createAluguelDAO();

    public IDaoBase<Categoria> createCategoriaDAO();

    public IDaoBase<Cliente> createClienteDAO();

    public IDaoBase<Filme> createFilmeDAO();

    public IDaoBase<Fita> createFitaDAO();
}
