package com.pateti.vhs.domain.dao.sqlite.impl;

import com.pateti.vhs.domain.dao.sqlite.SqliteBase;
import com.pateti.vhs.domain.entidades.Cliente;
import org.springframework.core.env.Environment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClienteDaoImpl extends SqliteBase<Cliente> {
    public ClienteDaoImpl(Environment env) {
        super(env);
    }

    private Cliente buildCliente(ResultSet res) throws SQLException {
        final int id = res.getInt("id");
        final String nome = res.getString("nome");
        final String cpf = res.getString("cpf");
        final int saldo = res.getInt("saldo");

        final Cliente c = new Cliente(id, nome, cpf);
        c.setSaldo(saldo);
        return c;
    }

    @Override
    public int inserir(Cliente ent) {
        final String sql = "INSERT INTO cliente (nome, cpf, saldo) VALUES (?, ?, ?)";

        return super.inserir(sql, ps -> {
            ps.setString(1, ent.getNome());
            ps.setString(2, ent.getCpf());
            ps.setInt(3, ent.getSaldo().movePointRight(2).intValue());
        });
    }

    @Override
    public boolean atualizar(Cliente ent) {
        final String sql = "UPDATE cliente SET nome=?, cpf=?, saldo=? WHERE id=?";

        return super.atualizar(sql, ps -> {
            ps.setString(1, ent.getNome());
            ps.setString(2, ent.getCpf());
            ps.setInt(3, ent.getSaldo().movePointRight(2).intValue());
            ps.setInt(4, ent.getId());
        });
    }

    @Override
    public boolean remover(int id) {
        final String sql = "DELETE FROM cliente WHERE id=?";

        return super.remover(sql, ps -> ps.setInt(1, id));
    }

    @Override
    public List<Cliente> listar() {
        final String sql = "SELECT id, nome, cpf, saldo FROM cliente";
        List<Cliente> lista = new ArrayList<>();

        super.listar(sql, rs -> {
            while (rs.next()) lista.add(buildCliente(rs));
        });

        return lista;
    }

    @Override
    public Cliente consultar(int id) {
        final String sql = "SELECT id, nome, cpf, saldo FROM cliente WHERE id=?";
        var wrapper = new Object() {
            Cliente cliente;
        };

        super.consultar(sql, ps -> ps.setInt(1, id), rs -> {
            if (rs.next()) wrapper.cliente = buildCliente(rs);
        });

        return wrapper.cliente;
    }
}
