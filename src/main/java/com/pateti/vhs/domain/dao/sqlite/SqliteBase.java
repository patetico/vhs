package com.pateti.vhs.domain.dao.sqlite;

import com.pateti.vhs.domain.dao.IDaoBase;
import org.springframework.core.env.Environment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class SqliteBase<T> implements IDaoBase<T> {
    private Environment env;

    protected SqliteBase(Environment env) {
        this.env = env;
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(String.format("jdbc:sqlite:%s", env.getProperty("vhs.sqlite.path")));
    }

    protected interface IStatementPreparer {
        void prepare(PreparedStatement ps) throws SQLException;
    }

    protected interface IResultsHandler {
        void handle(ResultSet rs) throws SQLException;
    }

    protected int inserir(String sql, IStatementPreparer preparer) {
        try (
            Connection conn = getConnection();
            Statement statement = conn.createStatement();
            PreparedStatement ps = conn.prepareStatement(sql)
        ) {
            preparer.prepare(ps);
            ps.execute();
            return statement.executeQuery("SELECT last_insert_rowid()").getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    protected boolean atualizar(String sql, IStatementPreparer preparer) {
        try (
            Connection conn = getConnection();
            PreparedStatement ps = conn.prepareStatement(sql)
        ) {
            preparer.prepare(ps);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    protected boolean remover(String sql, IStatementPreparer preparer) {
        try (
            Connection conn = getConnection();
            PreparedStatement ps = conn.prepareStatement(sql)
        ) {
            preparer.prepare(ps);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    protected void listar(String sql, IResultsHandler handler) {
        try (
            Connection conn = getConnection();
            ResultSet res = conn.createStatement().executeQuery(sql)
        ) {
            handler.handle(res);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void consultar(String sql, IStatementPreparer preparer, IResultsHandler handler) {
        try (
            Connection conn = getConnection();
            PreparedStatement ps = conn.prepareStatement(sql)
        ) {
            preparer.prepare(ps);
            ResultSet res = ps.executeQuery();
            handler.handle(res);
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
