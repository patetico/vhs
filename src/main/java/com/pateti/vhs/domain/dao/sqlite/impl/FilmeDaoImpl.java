package com.pateti.vhs.domain.dao.sqlite.impl;

import com.pateti.vhs.domain.dao.sqlite.SqliteBase;
import com.pateti.vhs.domain.entidades.Filme;
import org.springframework.core.env.Environment;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FilmeDaoImpl extends SqliteBase<Filme> {
    public FilmeDaoImpl(Environment env) {
        super(env);
    }

    /**
     * Prepara um statement
     *
     * @param ent entidade a ser injetada no statement
     * @param ps  sql statement na seguinte ordem: titulo, titulo_original, sinopse, duracao, censura, categoria
     * @throws SQLException erro no statement
     */
    private void preparaStatement(Filme ent, PreparedStatement ps) throws SQLException {
        ps.setString(1, ent.getTitulo());
        ps.setString(2, ent.getTituloOriginal());
        ps.setString(3, ent.getSinopse());
        ps.setInt(4, ent.getDuracao());
        ps.setInt(5, ent.getCensura());
        ps.setInt(6, ent.getCategoria().getId());
    }

    private Filme buildFilme(ResultSet res) throws SQLException {
        final int id = res.getInt("id");
        final String titulo = res.getString("titulo");
        final String tituloOriginal = res.getString("titulo_original");
        final String sinopse = res.getString("sinopse");
        final int duracao = res.getInt("duracao");
        final int censura = res.getInt("censura");
        final int catId = res.getInt("categoria");

        final Filme filme = new Filme(id, titulo);
        filme.setTituloOriginal(tituloOriginal);
        filme.setSinopse(sinopse);
        filme.setDuracao(duracao);
        filme.setCensura(censura);
        filme.setCategoriaId(catId);
        return filme;
    }

    @Override
    public int inserir(Filme ent) {
        final String sql = "INSERT INTO filme (titulo, titulo_original, sinopse, duracao, censura, categoria)" +
            " VALUES (?, ?, ?, ?, ?, ?)";

        return super.inserir(sql, ps -> preparaStatement(ent, ps));
    }

    @Override
    public boolean atualizar(Filme ent) {
        final String sql = "UPDATE filme SET " +
            "titulo=?, titulo_original=?, sinopse=?, duracao=?, censura=?, categoria=? " +
            "WHERE id=?";

        return super.atualizar(sql, ps -> {
            preparaStatement(ent, ps);
            ps.setInt(7, ent.getId());
        });
    }

    @Override
    public boolean remover(int id) {
        final String sql = "DELETE FROM filme WHERE id=?";

        return super.remover(sql, ps -> ps.setInt(1, id));
    }

    @Override
    public List<Filme> listar() {
        final String sql = "SELECT id, titulo, titulo_original, sinopse, duracao, censura, categoria FROM filme";
        List<Filme> lista = new ArrayList<>();

        super.listar(sql, rs -> {
            while (rs.next()) lista.add(buildFilme(rs));
        });

        return lista;
    }

    @Override
    public Filme consultar(int id) {
        final String sql = "SELECT id, titulo, titulo_original, sinopse, duracao, censura, categoria " +
            "FROM filme WHERE id=?";

        var wrapper = new Object() {
            Filme filme;
        };

        super.consultar(sql, ps -> ps.setInt(1, id), rs -> {
            if (rs.next()) wrapper.filme = buildFilme(rs);
        });

        return wrapper.filme;
    }
}
