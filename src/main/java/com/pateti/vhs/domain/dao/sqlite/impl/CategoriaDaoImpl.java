package com.pateti.vhs.domain.dao.sqlite.impl;

import com.pateti.vhs.domain.dao.sqlite.SqliteBase;
import com.pateti.vhs.domain.entidades.Categoria;
import org.springframework.core.env.Environment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CategoriaDaoImpl extends SqliteBase<Categoria> {
    public CategoriaDaoImpl(Environment env) {
        super(env);
    }

    private Categoria buildCategoria(ResultSet res) throws SQLException {
        final int id = res.getInt("id");
        final String nome = res.getString("nome");

        return new Categoria(id, nome);
    }

    @Override
    public int inserir(Categoria ent) {
        final String sql = "INSERT INTO categoria (nome) VALUES (?)";

        return super.inserir(sql, ps -> ps.setString(1, ent.getName()));
    }

    @Override
    public boolean atualizar(Categoria ent) {
        final String sql = "UPDATE categoria SET nome=? WHERE id=?";

        return super.atualizar(sql, ps -> {
            ps.setString(1, ent.getName());
            ps.setInt(2, ent.getId());
        });
    }

    @Override
    public boolean remover(int id) {
        final String sql = "DELETE FROM categoria WHERE id=?";

        return super.remover(sql, ps -> ps.setInt(1, id));
    }

    @Override
    public List<Categoria> listar() {
        final String sql = "SELECT id, nome FROM categoria";
        List<Categoria> lista = new ArrayList<>();

        super.listar(sql, rs -> {
            while (rs.next()) lista.add(buildCategoria(rs));
        });

        return lista;
    }

    @Override
    public Categoria consultar(int id) {
        final String sql = "SELECT id, nome FROM categoria WHERE id=?";
        var wrapper = new Object() {
            Categoria categoria;
        };

        super.consultar(sql, ps -> ps.setInt(1, id), rs -> {
            if (rs.next()) wrapper.categoria = buildCategoria(rs);
        });

        return wrapper.categoria;
    }
}
