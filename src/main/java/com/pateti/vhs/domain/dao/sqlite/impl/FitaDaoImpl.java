package com.pateti.vhs.domain.dao.sqlite.impl;

import com.pateti.vhs.domain.dao.sqlite.SqliteBase;
import com.pateti.vhs.domain.entidades.Fita;
import org.springframework.core.env.Environment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FitaDaoImpl extends SqliteBase<Fita> {
    public FitaDaoImpl(Environment env) {
        super(env);
    }

    private Fita buildFita(ResultSet res) throws SQLException {
        final int id = res.getInt("id");
        final int filmeId = res.getInt("filme");
        final boolean rebobinada = res.getInt("rebobinada") != 0;
        final String avarias = res.getString("avarias");

        final Fita fita = new Fita(id, filmeId);
        fita.setRebobinada(rebobinada);
        fita.setAvarias(avarias);

        return fita;
    }

    @Override
    public int inserir(Fita ent) {
        final String sql = "INSERT INTO fita (filme, rebobinada, avarias) VALUES (?, ?, ?)";

        return super.inserir(sql, ps -> {
            ps.setInt(1, ent.getFilme().getId());
            ps.setInt(2, ent.isRebobinada() ? 1 : 0);
            ps.setString(3, ent.getAvarias());
        });
    }

    @Override
    public boolean atualizar(Fita ent) {
        final String sql = "UPDATE fita SET filme=?, rebobinada=?, avarias=? WHERE id=?";

        return super.atualizar(sql, ps -> {
            ps.setInt(1, ent.getFilme().getId());
            ps.setInt(2, ent.isRebobinada() ? 1 : 0);
            ps.setString(3, ent.getAvarias());
            ps.setInt(4, ent.getId());
        });
    }

    @Override
    public boolean remover(int id) {
        final String sql = "DELETE FROM cliente WHERE id=?";

        return super.remover(sql, ps -> ps.setInt(1, id));
    }

    @Override
    public List<Fita> listar() {
        final String sql = "SELECT id, filme, rebobinada, avarias FROM fita";
        List<Fita> lista = new ArrayList<>();

        super.listar(sql, rs -> {
            while (rs.next()) lista.add(buildFita(rs));
        });

        return lista;
    }

    @Override
    public Fita consultar(int id) {
        final String sql = "SELECT id, filme, rebobinada, avarias FROM fita WHERE id=?";
        var wrapper = new Object() {
            Fita fita;
        };

        super.consultar(sql, ps -> ps.setInt(1, id), rs -> {
            if (rs.next()) wrapper.fita = buildFita(rs);
        });

        return wrapper.fita;
    }
}
