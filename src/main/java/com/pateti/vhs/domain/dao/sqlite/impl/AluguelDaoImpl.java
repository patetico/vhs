package com.pateti.vhs.domain.dao.sqlite.impl;

import com.pateti.vhs.domain.dao.sqlite.SqliteBase;
import com.pateti.vhs.domain.entidades.Aluguel;
import org.springframework.core.env.Environment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AluguelDaoImpl extends SqliteBase<Aluguel> {

    public AluguelDaoImpl(Environment env) {
        super(env);
    }

    private Aluguel buildAluguel(ResultSet res) throws SQLException {
        final int id = res.getInt("id");
        final int clienteId = res.getInt("cliente");
        final int fitaId = res.getInt("fita");
        final Date retirada = new Date(res.getLong("retirada") * 1000);
        final Date devolucao = new Date(res.getLong("devolucao") * 1000);
        final int taxaDiaria = res.getInt("taxa_diaria");
        final int valorTotal = res.getInt("valor_total");

        final Aluguel aluguel = new Aluguel(id, fitaId, clienteId, retirada, taxaDiaria);
        aluguel.setDevolucao(devolucao);
        aluguel.setValorTotal(valorTotal);
        return aluguel;
    }

    @Override
    public int inserir(Aluguel ent) {
        final String sql = "INSERT INTO aluguel (retirada) VALUES (?)";

        return super.inserir(sql, ps -> ps.setLong(1, ent.getRetirada().getTime() / 1000));
    }

    @Override
    public boolean atualizar(Aluguel ent) {
        final String sql = "UPDATE aluguel SET retirada=? WHERE id=?";

        return super.atualizar(sql, ps -> {
            ps.setLong(1, ent.getRetirada().getTime() / 1000);
            ps.setInt(2, ent.getId());
        });
    }

    @Override
    public boolean remover(int id) {
        final String sql = "DELETE FROM aluguel WHERE id=?";

        return super.remover(sql, ps -> ps.setInt(1, id));
    }

    @Override
    public List<Aluguel> listar() {
        final String sql = "SELECT id, cliente, fita, retirada, devolucao, taxa_diaria, valor_total FROM aluguel";
        List<Aluguel> lista = new ArrayList<>();

        super.listar(sql, rs -> {
            while (rs.next()) lista.add(buildAluguel(rs));
        });

        return lista;
    }

    @Override
    public Aluguel consultar(int id) {
        final String sql = "SELECT id, cliente, fita, retirada, devolucao, taxa_diaria, valor_total " +
            "FROM aluguel WHERE id=?";
        var wrapper = new Object() {
            Aluguel aluguel;
        };

        super.consultar(sql, ps -> ps.setInt(1, id), rs -> {
            if (rs.next()) wrapper.aluguel = buildAluguel(rs);
        });

        return wrapper.aluguel;
    }
}
