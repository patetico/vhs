package com.pateti.vhs.webapi.controllers;

import br.com.caelum.stella.format.CPFFormatter;
import br.com.caelum.stella.validation.CPFValidator;
import com.pateti.vhs.domain.dao.IDaoBase;
import com.pateti.vhs.domain.dao.IDaoImplFactory;
import com.pateti.vhs.domain.entidades.Cliente;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cliente")
public class ClienteController {
    private IDaoBase<Cliente> banco;

    private static CPFValidator cpfValidator = new CPFValidator();
    private static CPFFormatter cpfFormatter = new CPFFormatter();

    public ClienteController(IDaoImplFactory bancoFactory) {
        banco = bancoFactory.createClienteDAO();
    }

    @GetMapping("")
    public List<Cliente> index() {
        return banco.listar();
    }

    @GetMapping("/{id}")
    public Cliente consulta(@PathVariable int id) {
        return banco.consultar(id);
    }

    @GetMapping("/novo")
    public void novo(
        @RequestParam(name = "nome") String nome,
        @RequestParam(name = "cpf") String cpf,
        @RequestParam(name = "saldo", required = false, defaultValue = "0") int saldo // em centavos
    ) {
        cpfValidator.assertValid(cpfFormatter.unformat(cpf));
        final Cliente cliente = new Cliente(-1, nome, cpf);
        cliente.setSaldo(saldo);
        banco.inserir(cliente);
    }

}
