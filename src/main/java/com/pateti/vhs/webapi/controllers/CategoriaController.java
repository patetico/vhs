package com.pateti.vhs.webapi.controllers;

import com.pateti.vhs.domain.dao.IDaoBase;
import com.pateti.vhs.domain.dao.IDaoImplFactory;
import com.pateti.vhs.domain.entidades.Categoria;
import org.springframework.web.bind.annotation.*;

import javax.lang.model.type.NullType;
import java.util.List;

@RestController
@RequestMapping("/api/categoria")
public class CategoriaController {
    private IDaoBase<Categoria> banco;

    public CategoriaController(IDaoImplFactory bancoFactory) {
        banco = bancoFactory.createCategoriaDAO();
    }

    @GetMapping("")
    public BaseResponse<List<Categoria>> index() {
        return new BaseResponse<>(true, banco.listar());
    }

    @GetMapping("/{id}")
    public BaseResponse<Categoria> consulta(@PathVariable int id) {
        final Categoria cat = banco.consultar(id);
        final boolean ok = cat == null;
        return new BaseResponse<>(ok, ok ? "Categoria com id %d não encontrada" : null, cat);
    }

    @GetMapping("/update")
    public BaseResponse<Categoria> update(
        @RequestParam(name = "id") int id,
        @RequestParam(name = "nome") String nome
    ) {
        final Categoria categoria = new Categoria(id, nome);
        if (banco.atualizar(categoria)) return new BaseResponse<>(true, categoria);
        return new BaseResponse<>(false, String.format("A categoria com id %d não pode ser atualizada", id));
    }

    @GetMapping("/novo")
    public BaseResponse<Categoria> novo(@RequestParam(name = "nome") String nome) {
        final Categoria categoria = new Categoria(-1, nome);
        categoria.setId(banco.inserir(categoria));
        if (categoria.getId() >= 0) return new BaseResponse<>(true, categoria);
        return new BaseResponse<>(false, "Houve um erro ao tentar criar esta categoria. Talvez ela já exista?");
    }

    // @DeleteMapping("/remove/{id}")
    @GetMapping("/remove/{id}")
    public BaseResponse<NullType> remover(@PathVariable int id) {
        if (banco.remover(id)) return new BaseResponse<>(true, null);
        return new BaseResponse<>(false, "Ocorreu um problema.");
    }

}
