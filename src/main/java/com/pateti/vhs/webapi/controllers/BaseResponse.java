package com.pateti.vhs.webapi.controllers;

class BaseResponse<T> {
    private boolean ok;
    private String msg;
    private T dados;

    BaseResponse(boolean ok, T dados) {
        this(ok, null, dados);
    }

    BaseResponse(boolean ok, String msg) {
        this(ok, msg, null);
    }

    BaseResponse(boolean ok, String msg, T dados) {
        this.ok = ok;
        this.msg = msg;
        this.dados = dados;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getDados() {
        return dados;
    }

    public void setDados(T dados) {
        this.dados = dados;
    }
}
