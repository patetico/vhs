CREATE TABLE "cliente" (
    "id"	INTEGER PRIMARY KEY AUTOINCREMENT,
    "nome"	TEXT NOT NULL,
    "cpf"	TEXT NOT NULL,
    "saldo"	INTEGER DEFAULT 0
);

CREATE TABLE "categoria" (
    "id"	INTEGER PRIMARY KEY AUTOINCREMENT,
    "nome"	TEXT NOT NULL UNIQUE
);

CREATE TABLE "filme" (
    "id"	INTEGER PRIMARY KEY AUTOINCREMENT,
    "titulo"	TEXT NOT NULL,
    "titulo_original"	TEXT NOT NULL,
    "sinopse"	TEXT,
    "duracao"	INTEGER,
    "censura"	INTEGER,
    "categoria"	INTEGER,
    FOREIGN KEY("categoria") REFERENCES "categoria"("id")
);

CREATE TABLE "fita" (
    "id"	INTEGER PRIMARY KEY AUTOINCREMENT,
    "filme"	INTEGER NOT NULL,
    "rebobinada"	INTEGER DEFAULT 0,
    "avarias"	TEXT,
    FOREIGN KEY("filme") REFERENCES "filme"("id")
);

CREATE TABLE "aluguel" (
    "id"	INTEGER PRIMARY KEY AUTOINCREMENT,
    "cliente"	INTEGER NOT NULL,
    "fita"	INTEGER NOT NULL,
    "retirada"	INTEGER NOT NULL,
    "devolucao"	INTEGER,
    "taxa_diaria"	INTEGER NOT NULL,
    "valor_total"	INTEGER,
    FOREIGN KEY("fita") REFERENCES "fita"("id"),
    FOREIGN KEY("cliente") REFERENCES "cliente"("id")
);
